--
--    Copyright 2015-2019 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--
------------------------------------------------------------------------------------------
--sftp

insert into sftp (host, port, user_name, password, upload_path)
values ('111.229.25.107', 22, 'root', 'liujunxi521.', '/app/');
insert into sftp (host, port, user_name, password, upload_path)
values ('10.16.3.110', 1990, 'chinaamc1', 'test1', '/upload/');
insert into sftp (host, port, user_name, password, upload_path)
values ('10.16.3.111', 1991, 'chinaamc2', 'test2', '/upload/');
insert into sftp (host, port, user_name, password, upload_path)
values ('10.16.3.112', 1992, 'chinaamc3', 'test3', '/upload/');
insert into sftp (host, port, user_name, password, upload_path)
values ('10.16.3.113', 1993, 'chinaamc4', 'test4', '/upload/');
insert into sftp (host, port, user_name, password, upload_path)
values ('10.16.3.114', 1994, 'chinaamc5', 'test5', '/upload/');

------------------------------------------------------------------------------------------
--db
insert into db (url, user_name, password, driver_class_name)
values ('jdbc:mysql://localhost:3306/mysql?useSSL=false', 'root', '123456', 'com.mysql.cj.jdbc.Driver');
insert into db (url, user_name, password, driver_class_name)
values ('jdbc:mysql://localhost:3307/mysql?useSSL=false', 'root', '123456', 'com.mysql.cj.jdbc.Driver');
insert into db (url, user_name, password, driver_class_name)
values ('jdbc:h2:mem:testdb', 'sa', '', 'org.h2.Driver');
------------------------------------------------------------------------------------------
--inst
insert into institution (inst_id, name)
values ('ths', '同花顺');
insert into institution (inst_id, name)
values ('zxyh', '中信银行');
insert into institution (inst_id, name)
values ('360', '360');
