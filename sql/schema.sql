--
--    Copyright 2015-2019 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

drop table if exists sftp;

create table sftp
(
    id          int auto_increment primary key,
    host        varchar(20),
    port        int,
    user_name   varchar(100),
    password    varchar(100),
    upload_path varchar(200)
);


drop table if exists db;
create table db
(
    id                int auto_increment primary key,
    url               varchar(500),
    user_name         varchar(100),
    password          varchar(100),
    driver_class_name varchar(300)
);

drop table if exists template;
create table template
(
    id            int auto_increment primary key,
    inst_id          varchar(20),
    template_name varchar(500),
    create_time   timestamp,
    update_time   timestamp,
    template      text
);

drop table if exists institution;
create table institution
(
    id   int auto_increment primary key,
    inst_id varchar(20),
    name varchar(100)
);