# Getting Started

### 启动项目
访问 http://localhost:8093/h2-console
JDBC-URL：  jdbc:h2:file:./data/g-file
UserName：sa
Password

将sql文件夹下的sql脚本在控制台执行
数据库db文件在data目录，支持移动数据

### 访问主页
http://localhost:8093/
如果想让别人访问你的g-file 修改此文件内容static/js/http.js
```
layui.define(function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
let obj = {
api:"http://localhost:8093/"  //如果想让别人访问你的g-file 这个要改成部署项目的实际ip
};
//输出test接口
exports('http', obj);
});
```
