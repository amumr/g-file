package com.example.gfile.exception;

public class GFileException extends RuntimeException {

    public GFileException(String message) {
        super(message);
    }

    public GFileException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
