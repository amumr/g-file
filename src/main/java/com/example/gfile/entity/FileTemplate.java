package com.example.gfile.entity;

import lombok.Data;

import java.util.List;


/**
 * 文件内容模版
 */
@Data
public class FileTemplate {

    //自定义文件头
    public static final String CUSTOM_CONTENT = "1";
    //自定义文件字段
    public static final String CUSTOM_FILED = "2";

    //分隔符
    private String separator;
    //文件编码
    private String fileEncoding;
    //字段左占位
    private boolean leftAlign;
    //字段右占位
    private boolean rightAlign;
    //包含明细数据
    private boolean containDetailData;
    //自定义字段
    private String fileHeadCustomizeSelect;
    //机构Id
    private String instId;

    //文本域文件头
    private CustomizeContent customizeContent;
    //文件名
    private FileName fileName;
    //自定义文件头
    private FileHead fileHead;

    //文件明细
    private List<FileDetail> fileDetailList;

    //是否分隔符结尾
    private boolean detailSeparatorEnd;

    //结尾内容
    private String endTextarea;


}
