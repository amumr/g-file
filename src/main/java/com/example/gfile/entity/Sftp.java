package com.example.gfile.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Sftp {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String host;
    private int port;
    private String userName;
    private String password;
    private String uploadPath;

}
