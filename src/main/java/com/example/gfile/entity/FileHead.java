package com.example.gfile.entity;

import lombok.Data;

import java.util.List;

@Data
public class FileHead {

    //是否分隔符结尾
    private boolean headSeparatorEnd;

    //字段内容
    private List<Filed> filedList;
}
