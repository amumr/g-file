package com.example.gfile.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class FileName {

    private String fileName;
    private int fileCount;
    private String batchNo;
    private String fileNameDate;
    private boolean fileCompressSwitch;
    private String fileCompressName;
    private boolean preFileSwitch;
    private String preFileNameSuffix;

    @JsonIgnore
    private List<String> listBatchNo;
    @JsonIgnore
    private List<String> listFileName;


}
