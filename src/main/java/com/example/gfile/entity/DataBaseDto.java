package com.example.gfile.entity;

import lombok.Data;

@Data
public class DataBaseDto {

    private String username;
    private String password;
    private String driverClassName;
    private String url;

}
