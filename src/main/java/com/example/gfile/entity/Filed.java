package com.example.gfile.entity;

import lombok.Data;

@Data
public class Filed {

    //字段类型
    private String type;
    //字段内容
    private String content;
    //字段长度
    private int length;
    //前后缀
    private String affix;
    //整数
    private int amount;
    //金额小数位
    private int scale;
    //日期
    private String date;

    //数据库Id
    private String sqlId;
    //数据库数据行索引
    private int lineIndex;
    //数据库数据字段名
    private String column;

    //集合数据
    private ChainFiled chainFiled;


}
