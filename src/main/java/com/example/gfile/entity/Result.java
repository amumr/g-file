package com.example.gfile.entity;

import lombok.Data;

@Data
public class Result<T> {

    public static final String SUCCESS_MESSAGE = "success";
    public static final int SUCCESS_CODE = 0;
    public static final int FAIL_CODE = 9;

    private int code;

    private String msg;

    private int count;

    private T data;

    public Result() {
        this.msg = SUCCESS_MESSAGE;
    }

    public Result(T data) {
        this.data = data;
        this.code = SUCCESS_CODE;
        this.msg = SUCCESS_MESSAGE;
    }

    public Result(int code, String message) {
        this.code = code;
        this.msg = message;
    }

    public static Result success() {
        return new Result<>();
    }

    public static <T> Result<T> success(T data) {
        return new Result<>(data);
    }

    public static Result fail(String message) {
        return new Result(FAIL_CODE, message);
    }

}
