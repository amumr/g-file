package com.example.gfile.entity;

import lombok.Data;

import java.util.List;

@Data
public class ChainFiled {

    //集合前缀
    private String collPrefix;
    //集合后缀
    private String collSuffix;
    //子字段数
    private int collCount;
    //集合内元素个数
    private int collElementCount;
    //集合内元素分隔符
    private String collElementSeparator;

    //子字段前缀
    private String chainPrefix;
    //子字段后缀
    private String chainSuffix;
    //分隔符
    private String separator;

    //子字段
    private List<Filed> chainFiledList;
}
