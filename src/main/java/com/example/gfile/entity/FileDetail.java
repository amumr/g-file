package com.example.gfile.entity;

import lombok.Data;

import java.util.List;

@Data
public class FileDetail{

    //明细数据量
    private int fileDetailDataCount;

    //数据库sql
    private String sql;

    //数据库id-在db表中存在的
    private String dbId;

    private List<Filed> filedList;
}
