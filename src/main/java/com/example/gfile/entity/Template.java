package com.example.gfile.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class Template {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String instId;
    private String templateName;
    private Date createTime;
    private Date updateTime;
    private String template;

}
