package com.example.gfile.entity;


import lombok.Data;

@Data
public class FileVo {

    private String fileName;
    private String path;
    private String size;
    private String createTime;
    private String lastUpdateTime;

}
