package com.example.gfile.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Institution {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String instId;
    private String name;
}
