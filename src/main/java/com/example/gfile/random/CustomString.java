package com.example.gfile.random;

import com.example.gfile.entity.Filed;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class CustomString implements IRandomData {

    @Override
    public String type() {
        return CUSTOM_STRING_TYPE;
    }


    @Override
    public Function<Filed, String> data() {
        return filed -> {
            String content = filed.getContent();
            if (content.contains("|")) {
                String[] split = content.split("\\|");
                int random = RandomUtils.nextInt(0, split.length);
                return split[random];
            } else {
                return content;
            }
        };
    }
}
