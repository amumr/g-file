package com.example.gfile.random;

import com.example.gfile.entity.Filed;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class RandomDate implements IRandomData {

    @Override
    public String type() {
        return DATE_TYPE;
    }

    @Override
    public Function<Filed, String> data() {
        return filed -> filed.getDate();
    }
}
