package com.example.gfile.random;

import com.example.gfile.entity.Filed;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class RandomEmail implements IRandomData {

    @Override
    public String type() {
        return EMAIL_TYPE;
    }

    @Override
    public Function<Filed, String> data() {
        return filed -> {
            String email = "%s@qq.com";
            int length = filed.getLength();
            if (length > 10) {
                String data = RandomStringUtils.randomNumeric(length - 7);
               return String.format(email,data);
            }
            return "8888@qq.com";
        };
    }
}
