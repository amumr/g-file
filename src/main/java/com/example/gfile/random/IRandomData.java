package com.example.gfile.random;

import com.example.gfile.entity.Filed;

import java.util.function.Function;

public interface IRandomData {

    //自定义字符串类型
    String CUSTOM_STRING_TYPE = "0";
    //uuid类型
    String UUID_TYPE = "1";
    //数字类型
    String DIGITAL_TYPE = "2";
    //金额类型
    String AMOUNT_TYPE = "3";
    //地址类型
    String ADDRESS_TYPE = "4";
    //邮箱类型
    String EMAIL_TYPE = "5";
    //日期类型
    String DATE_TYPE = "6";
    //集合数据
    String LIST_TYPE = "7";
    //递增行号
    String INCREASE_NUM_TYPE = "8";
    //数据库字段
    String DB_COLUMN_TYPE = "9";

    String type();

    Function<Filed, String> data();
}
