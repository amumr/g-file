package com.example.gfile.random;

import com.example.gfile.entity.Filed;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
public class RandomAddress implements IRandomData {

    public static final List<String> LIST_ADDRESS = List.of("八达岭-万里长城",
            "西安-秦始皇兵马俑", "四川-稻城亚丁", "玻利维亚-天空之镜", "洛阳-老君山", "北京-国家森林公园",
            "心之所想，必不能往矣");


    @Override
    public String type() {
        return ADDRESS_TYPE;
    }

    @Override
    public Function<Filed, String> data() {
        return filed -> {
            int nextInt = RandomUtils.nextInt(0, LIST_ADDRESS.size());
            return LIST_ADDRESS.get(nextInt);
        };
    }
}
