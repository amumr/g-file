package com.example.gfile.random;

import com.example.gfile.entity.Filed;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

@Service
public class RandomAmount implements IRandomData {
    @Override
    public String type() {
        return AMOUNT_TYPE;
    }

    @Override
    public Function<Filed, String> data() {
        return filed -> {
            int amount = filed.getAmount();
            int scale = filed.getScale();
            double content = RandomUtils.nextDouble(0, amount);
            BigDecimal bigDecimal = new BigDecimal(String.valueOf(content));
            if (scale > 0) {
                return bigDecimal.setScale(scale, RoundingMode.DOWN).toString();
            }
            return String.valueOf(bigDecimal.intValue());
        };
    }
}
