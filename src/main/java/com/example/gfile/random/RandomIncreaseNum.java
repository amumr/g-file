package com.example.gfile.random;

import com.example.gfile.entity.Filed;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

@Service
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class RandomIncreaseNum implements IRandomData {

    AtomicInteger incr = new AtomicInteger(1);

    @Override
    public String type() {
        return INCREASE_NUM_TYPE;
    }

    @Override
    public Function<Filed, String> data() {
        return filed -> {
            int i = incr.get();
            incr.getAndIncrement();
            return String.valueOf(i);
        };
    }
}
