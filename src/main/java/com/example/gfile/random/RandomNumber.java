package com.example.gfile.random;

import com.example.gfile.entity.Filed;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class RandomNumber implements IRandomData {

    @Override
    public String type() {
        return DIGITAL_TYPE;
    }

    @Override
    public Function<Filed, String> data() {
        return filed -> {
            String numeric = RandomStringUtils.randomNumeric(48);
            int length = filed.getLength();
            if (length > 0 ) {
                numeric = StringUtils.substring(numeric, 0, length);
            }
            String affix = filed.getAffix();
            if (StringUtils.isNotEmpty(affix)) {
                String[] split = affix.split("\\|");
                if (StringUtils.startsWith(affix, "left|")) {
                    numeric = split[1] + numeric;
                }

                if (StringUtils.startsWith(affix, "right|")) {
                    numeric = numeric + split[1];
                }
            }
            return numeric;
        };
    }
}
