package com.example.gfile.random;

import com.example.gfile.entity.Filed;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.function.Function;

@Service
public class RandomUUid implements IRandomData {


    @Override
    public String type() {
        return UUID_TYPE;
    }

    @Override
    public Function<Filed, String> data() {
        return filed -> {
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            int length = filed.getLength();
            if (length > 0) {
                uuid = StringUtils.substring(uuid, 0, length);
            }
            String affix = filed.getAffix();
            if (StringUtils.isNotEmpty(affix)) {
                String[] split = affix.split("\\|");
                if (StringUtils.startsWith(affix, "left|")) {
                    uuid = split[1] + uuid;
                }

                if (StringUtils.startsWith(affix, "right|")) {
                    uuid = uuid + split[1];
                }
            }
            return uuid;
        };
    }
}
