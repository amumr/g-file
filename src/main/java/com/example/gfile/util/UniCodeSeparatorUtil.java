package com.example.gfile.util;

public class UniCodeSeparatorUtil {

    public static String convert(String separator) {
        if (separator.contains("US")) {
            return String.valueOf('\u001F');
        }

        if (separator.contains("GS")) {
            return String.valueOf('\u001D');
        }
        return separator;
    }
}
