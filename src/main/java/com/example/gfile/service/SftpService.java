package com.example.gfile.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.gfile.entity.Sftp;
import com.example.gfile.mapper.SftpMapper;
import com.example.gfile.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SftpService {

    @Autowired
    private SftpMapper sftpMapper;

    public void save(String data) {
        Sftp sftp = JsonUtil.jsonToObject(data, Sftp.class);
        sftpMapper.insert(sftp);
    }

    public List<Sftp> getAll() {
        return sftpMapper.selectList(null);
    }

    public List<Sftp> getPage(int page, int limit) {
        Page<Sftp> sftpPage = new Page(page, limit);
        Page<Sftp> selectPage = sftpMapper.selectPage(sftpPage, null);
        return selectPage.getRecords();
    }

    public int update(String data) {
        Sftp sftp = JsonUtil.jsonToObject(data, Sftp.class);
        return sftpMapper.updateById(sftp);
    }

    public Sftp selectById(String id) {
        return sftpMapper.selectById(id);
    }

    public int delete(String id) {
        return sftpMapper.deleteById(id);
    }
}
