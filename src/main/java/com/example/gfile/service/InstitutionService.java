package com.example.gfile.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.gfile.entity.Institution;
import com.example.gfile.mapper.InstitutionMapper;
import com.example.gfile.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InstitutionService {

    @Autowired
    private InstitutionMapper mapper;

    public void save(String data){
        Institution institution = JsonUtil.jsonToObject(data, Institution.class);
        mapper.insert(institution);
    }

    public List<Institution> getAll() {
        return mapper.selectList(null);
    }

    public List<Institution> getPage(int page, int limit) {
        Page<Institution> institutionPage = new Page(page,limit);
        Page<Institution> selectPage = mapper.selectPage(institutionPage, null);
        return  selectPage.getRecords();
    }

    public int update(String data) {
        Institution institution = JsonUtil.jsonToObject(data, Institution.class);
        return mapper.updateById(institution);
    }

    public Institution selectById(String id) {
        return mapper.selectById(id);
    }

    public int delete(String id) {
        return mapper.deleteById(id);
    }
}
