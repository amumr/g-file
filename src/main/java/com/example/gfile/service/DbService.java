package com.example.gfile.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.gfile.entity.Db;
import com.example.gfile.mapper.DbMapper;
import com.example.gfile.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DbService {

    @Autowired
    private DbMapper DbMapper;

    public void save(String data){
        Db Db = JsonUtil.jsonToObject(data, Db.class);
        DbMapper.insert(Db);
    }

    public List<Db> getAll() {
        return DbMapper.selectList(null);
    }

    public List<Db> getPage(int page, int limit) {
        Page<Db> DbPage = new Page(page,limit);
        Page<Db> selectPage = DbMapper.selectPage(DbPage, null);
        return  selectPage.getRecords();
    }

    public int update(String data) {
        Db Db = JsonUtil.jsonToObject(data, Db.class);
       return DbMapper.updateById(Db);
    }

    public Db selectById(String id) {
        return DbMapper.selectById(id);
    }

    public int delete(String id) {
        return DbMapper.deleteById(id);
    }

}
