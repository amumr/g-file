package com.example.gfile.service;

import com.example.gfile.entity.FileVo;
import com.example.gfile.entity.Sftp;
import com.example.gfile.exception.GFileException;
import com.example.gfile.util.JSchUtil;
import com.example.gfile.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.function.Consumer;

@Service
@Slf4j
public class UploadFIleService {

    @Autowired
    private SftpService sftpService;

    public void uploadFile(String sftpId, MultipartFile file) {
        try {
            Sftp sftp = sftpService.selectById(sftpId);
            JSchUtil jSchUtil = new JSchUtil();
            JSchUtil.SftpDto sftpDto = new JSchUtil.SftpDto();
            sftpDto.setSftp(sftp);
            sftpDto.setMultipartFile(file);
            Consumer<JSchUtil.SftpDto> consumer = jSchUtil.initialize()
                    .andThen(jSchUtil.mkdir())
                    .andThen(jSchUtil.uploadFromHtmlFile())
                    .andThen(jSchUtil.close());
            consumer.accept(sftpDto);
        } catch (Exception e) {
            throw new GFileException(e.getMessage(), e);
        }
    }

    public void upload(String data, String sftpId) {
        try {
            FileVo fileVo = JsonUtil.jsonToObject(data, FileVo.class);
            Sftp sftp = sftpService.selectById(sftpId);
            JSchUtil jSchUtil = new JSchUtil();
            JSchUtil.SftpDto sftpDto = new JSchUtil.SftpDto();
            sftpDto.setSftp(sftp);
            File file = new File(fileVo.getPath() + fileVo.getFileName());
            sftpDto.setFile(file);
            Consumer<JSchUtil.SftpDto> consumer = jSchUtil.initialize()
                    .andThen(jSchUtil.mkdir())
                    .andThen(jSchUtil.uploadFile())
                    .andThen(jSchUtil.close());
            consumer.accept(sftpDto);
        } catch (Exception e) {
            throw new GFileException(e.getMessage(), e);
        }
    }

}
