package com.example.gfile.handle;

import com.example.gfile.entity.Result;
import com.example.gfile.exception.GFileException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
@Slf4j
//全局controller异常捕获
public class GlobalControllerExceptionHandle {

    @ExceptionHandler(GFileException.class)
    public Result doStepException(HttpServletRequest request, GFileException e) {
        log.error(e.getMessage(), request);
        return Result.fail(e.getMessage());
    }

    @ExceptionHandler(Throwable.class)
    public Result throwable(HttpServletRequest request, Throwable throwable) {
        log.error(throwable.getMessage(), request);
        return Result.fail(throwable.getMessage());
    }
}
