package com.example.gfile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GFileApplication {

    public static void main(String[] args) {
        SpringApplication.run(GFileApplication.class, args);
    }


}
