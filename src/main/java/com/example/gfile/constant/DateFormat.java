package com.example.gfile.constant;

import com.example.gfile.exception.GFileException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormat {

    public static final String YYYY = "yyyy";
    public static final String MM = "MM";
    public static final String DD = "dd";
    public static final String HH = "HH";
    public static final String MINUTE = "mm";
    public static final String SS = "ss";

    public static final String FILE_NAME_DATE = "yyyy-MM-dd HH:mm:ss";

    public static String formatStr(Date date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public static String formatStr(Long date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    public static Date formatDate(Date date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        String DateStr = format.format(date);
        try {
            return  format.parse(DateStr);
        } catch (ParseException e) {
            throw new GFileException(e.getMessage(), e);
        }
    }

}
