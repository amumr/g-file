package com.example.gfile.controller;

import com.example.gfile.entity.Result;
import com.example.gfile.service.SqlMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class LoadDbController {

    @Resource
    private SqlMapperService sqlMapperService;

    @RequestMapping("/loadDb")
    public Result loadDb(String data, String id) {
        List<List<Map<String, Object>>> lists = sqlMapperService.executeQuerySql(data, id);
        return Result.success(lists);
    }
}
