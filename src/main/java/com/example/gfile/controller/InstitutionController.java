package com.example.gfile.controller;

import com.example.gfile.entity.Institution;
import com.example.gfile.entity.Result;
import com.example.gfile.service.InstitutionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/inst")
public class InstitutionController {


    @Autowired
    private InstitutionService institutionService;

    @RequestMapping("/save")
    public Result save(String data) {
        institutionService.save(data);
        return Result.success("ok");
    }

    @RequestMapping("/getAll")
    public Result getAll() {
        List<Institution> list = institutionService.getAll();
        Result<List<Institution>> result = new Result<>();
        result.setMsg(Result.SUCCESS_MESSAGE);
        result.setCount(list.size());
        result.setData(list);
        return result;
    }

    @RequestMapping("/getPage")
    public Result getPage(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        List<Institution> list = institutionService.getPage(page, limit);
        List<Institution> all = institutionService.getAll();
        Result<List<Institution>> result = new Result<>();
        result.setMsg(Result.SUCCESS_MESSAGE);
        result.setCount(all.size());
        result.setData(list);
        return result;
    }

    @RequestMapping("/update")
    public Result update(String data) {
        int update = institutionService.update(data);
        if (update > 0) {
            return Result.success();
        } else {
            return Result.fail("未匹配到数据");
        }
    }

    @RequestMapping("/delete")
    public Result delete(String id) {
        int delete = institutionService.delete(id);
        if (delete > 0) {
            return Result.success();
        } else {
            return Result.fail("未匹配到数据");
        }
    }

}
