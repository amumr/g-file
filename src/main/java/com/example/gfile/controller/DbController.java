package com.example.gfile.controller;

import com.example.gfile.entity.Db;
import com.example.gfile.entity.Result;
import com.example.gfile.service.DbService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/db")
@RequiredArgsConstructor
public class DbController {

    @Autowired
    private DbService DbService;

    @RequestMapping("/save")
    public Result save(String data) {
        DbService.save(data);
        return Result.success("ok");
    }

    @RequestMapping("/getAll")
    public Result getAll() {
        List<Db> list = DbService.getAll();
        Result<List<Db>> result = new Result<>();
        result.setMsg(Result.SUCCESS_MESSAGE);
        result.setCount(list.size());
        result.setData(list);
        return result;
    }

    @RequestMapping("/getPage")
    public Result getPage(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        List<Db> list = DbService.getPage(page, limit);
        List<Db> all = DbService.getAll();
        Result<List<Db>> result = new Result<>();
        result.setMsg(Result.SUCCESS_MESSAGE);
        result.setCount(all.size());
        result.setData(list);
        return result;
    }

    @RequestMapping("/update")
    public Result update(String data) {
        int update = DbService.update(data);
        if (update > 0) {
            return Result.success();
        } else {
            return Result.fail("未匹配到数据");
        }
    }

    @RequestMapping("/delete")
    public Result delete(String id) {
        int delete = DbService.delete(id);
        if (delete > 0) {
            return Result.success();
        } else {
            return Result.fail("未匹配到数据");
        }
    }
}
