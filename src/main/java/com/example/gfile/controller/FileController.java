package com.example.gfile.controller;

import com.example.gfile.entity.FileTemplate;
import com.example.gfile.entity.Result;
import com.example.gfile.entity.Template;
import com.example.gfile.service.FileJsonParseService;
import com.example.gfile.service.FileService;
import com.example.gfile.util.JsonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController()
@RequestMapping("/file")
@RequiredArgsConstructor
public class FileController {


    @Value("${root-directory}")
    private String rootDirectory;
    @Resource
    private FileService fileService;
    @Resource
    private FileJsonParseService parseService;

    @RequestMapping("/generate")
    public Result generate(String data, String fileTemplateName) {
        log.info(data);
        FileTemplate fileTemplate = parseService.parse(data);
        if (StringUtils.isNotEmpty(fileTemplateName)) {
            fileService.save(fileTemplateName, fileTemplate);
        }
        fileService.process(fileTemplate);
        return Result.success(rootDirectory);
    }

    @RequestMapping("/getAllFile")
    public Result getAllFile() {
        return fileService.getAllFile();
    }

    @RequestMapping("/deleteFile")
    public Result deleteFile(String fileName, String path) {
        return fileService.deleteFile(fileName, path);
    }

    @RequestMapping("/getPage")
    public Result getPage(@RequestParam("instId") String instId, @RequestParam("page") int page, @RequestParam("limit") int limit) {
        List<Template> list = fileService.getPage(instId, page, limit);
        List<Template> all = fileService.getAll(instId);
        Result<List<Template>> result = new Result<>();
        result.setMsg(Result.SUCCESS_MESSAGE);
        result.setCount(all.size());
        result.setData(list);
        return result;
    }

    @RequestMapping("/update")
    public Result update(String data) {
        int update = fileService.update(data);
        if (update > 0) {
            return Result.success();
        } else {
            return Result.fail("未匹配到数据");
        }
    }

    @RequestMapping("/delete")
    public Result delete(String id) {
        int delete = fileService.delete(id);
        if (delete > 0) {
            return Result.success();
        } else {
            return Result.fail("未匹配到数据");
        }
    }

    @RequestMapping("/rest")
    public Result rest(String id) {
        Template template = fileService.selectById(id);
        FileTemplate fileTemplate = JsonUtil.jsonToObject(template.getTemplate(), FileTemplate.class);
        fileService.process(fileTemplate);
        return Result.success(rootDirectory);
    }

}

