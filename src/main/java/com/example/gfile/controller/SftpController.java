package com.example.gfile.controller;

import com.example.gfile.entity.Result;
import com.example.gfile.entity.Sftp;
import com.example.gfile.service.SftpService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/sftp")
@RequiredArgsConstructor
public class SftpController {

    @Autowired
    private SftpService sftpService;

    @RequestMapping("/save")
    public Result save(String data) {
        sftpService.save(data);
        return Result.success("ok");
    }

    @RequestMapping("/getAll")
    public Result getAll() {
        List<Sftp> list = sftpService.getAll();
        Result<List<Sftp>> result = new Result<>();
        result.setMsg(Result.SUCCESS_MESSAGE);
        result.setCount(list.size());
        result.setData(list);
        return result;
    }

    @RequestMapping("/getPage")
    public Result getPage(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        List<Sftp> list = sftpService.getPage(page, limit);
        List<Sftp> all = sftpService.getAll();
        Result<List<Sftp>> result = new Result<>();
        result.setMsg(Result.SUCCESS_MESSAGE);
        result.setCount(all.size());
        result.setData(list);
        return result;
    }

    @RequestMapping("/update")
    public Result update(String data) {
        int update = sftpService.update(data);
        if (update > 0) {
            return Result.success();
        } else {
            return Result.fail("未匹配到数据");
        }
    }

    @RequestMapping("/delete")
    public Result delete(String id) {
        int delete = sftpService.delete(id);
        if (delete > 0) {
            return Result.success();
        } else {
            return Result.fail("未匹配到数据");
        }
    }
}
