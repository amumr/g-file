package com.example.gfile.controller;

import com.example.gfile.entity.Result;
import com.example.gfile.exception.GFileException;
import com.example.gfile.service.UploadFIleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Slf4j
public class UploadFileController {

    @Autowired
    private UploadFIleService uploadFIleService;

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public Result uploadFile(@RequestParam("file") MultipartFile file, String sftpId) {
        log.info("文件上传参数{},{}", sftpId, file.getOriginalFilename());

        if (StringUtils.isEmpty(sftpId)) {
            throw new GFileException("sftp is null");
        }
        uploadFIleService.uploadFile(sftpId, file);
        return Result.success(file.getOriginalFilename());
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Result upload(String data, String sftpId) {
        log.info("文件上传参数{},{}", sftpId, data);

        if (StringUtils.isEmpty(sftpId)) {
            throw new GFileException("sftp is null");
        }
        uploadFIleService.upload(data, sftpId);
        return Result.success();
    }
}
