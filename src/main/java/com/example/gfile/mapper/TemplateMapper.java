package com.example.gfile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.gfile.entity.Template;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface TemplateMapper extends BaseMapper<Template> {

}
