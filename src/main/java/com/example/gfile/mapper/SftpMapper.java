package com.example.gfile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.gfile.entity.Sftp;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface SftpMapper extends BaseMapper<Sftp> {

}
