package com.example.gfile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.gfile.entity.Db;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface DbMapper extends BaseMapper<Db> {

}
