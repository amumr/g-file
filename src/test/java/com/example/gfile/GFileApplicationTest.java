package com.example.gfile;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.gfile.entity.Db;
import com.example.gfile.mapper.DbMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;


@SpringBootTest
class GFileApplicationTest {

    @Resource
    private DbMapper dbMapper;

    @Test
    void contextLoads() throws Exception {
        LambdaQueryWrapper<Db> queryWrapper = new LambdaQueryWrapper();
        List<Db> list = dbMapper.selectList(queryWrapper);
        System.out.println(list.size());

        File file = new File("/Users/sky/Downloads/Generate/template-2022-10-26.txt");
        System.out.println(file.getAbsolutePath());
        System.out.println(file.getPath());
        System.out.println(file.getParent());
        System.out.println(file.getFreeSpace());
    }


}