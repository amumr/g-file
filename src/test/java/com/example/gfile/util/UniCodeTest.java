package com.example.gfile.util;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UniCodeTest {



    @Test
    public void unicodeConvert(){
        String sourceData = "US";
        String unicodeEncode = unicodeEncode(sourceData);
        System.out.println("编码结果：");
        System.out.println(unicodeEncode);

        String unicodeDecode = unicodeDecode("\u001F");
        System.out.println("解码结果：");
        System.out.println(unicodeDecode);//这是原始的数据！！！

    }


    /**
     * @Title: unicodeEncode
     * @Description: unicode编码
     * @param string
     * @return
     */
    public static String unicodeEncode(String string) {
        char[] utfBytes = string.toCharArray();
        String unicodeBytes = "";
        for (int i = 0; i < utfBytes.length; i++) {
            String hexB = Integer.toHexString(utfBytes[i]);
            if (hexB.length() <= 2) {
                hexB = "00" + hexB;
            }
            unicodeBytes = unicodeBytes + "\\u" + hexB;
        }
        return unicodeBytes;
    }


    /**
     * @Title: unicodeDecode
     * @Description: unicode解码
     * @param string
     * @return
     */
    public static String unicodeDecode(String string) {
        Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
        Matcher matcher = pattern.matcher(string);
        char ch;
        while (matcher.find()) {
            ch = (char) Integer.parseInt(matcher.group(2), 16);
            string = string.replace(matcher.group(1), ch + "");
        }
        return string;
    }

}
