package com.example.gfile.util;

import org.junit.jupiter.api.Test;

import java.util.Map;

class JsonUtilTest {

    @Test
    void jsonToObject() {
        String json = "{\n" +
                "    \"separator\": \"\",\n" +
                "    \"fileEncoding\": \"UTF-8\",\n" +
                "    \"containDetailData\": \"on\",\n" +
                "    \"fileName\": \"111111\",\n" +
                "    \"batchNo\": \"\",\n" +
                "    \"fileHeadCustomizeSelect\": \"1\",\n" +
                "    \"customizeTextarea\": \"\",\n" +
                "    \"fileDetailFiledCount\": \"\",\n" +
                "    \"fileDetailDataCount\": \"\",\n" +
                "    \"detailRandomData-0\": \"1\",\n" +
                "    \"detailFiledLength-0\": \"\",\n" +
                "    \"detailRandomData-1\": \"1\",\n" +
                "    \"detailFiledLength-1\": \"\",\n" +
                "    \"detailRandomData-2\": \"1\",\n" +
                "    \"detailFiledLength-2\": \"\",\n" +
                "    \"detailRandomData-3\": \"1\",\n" +
                "    \"detailFiledLength-3\": \"\",\n" +
                "    \"detailRandomData-4\": \"1\",\n" +
                "    \"detailFiledLength-4\": \"\",\n" +
                "    \"detailRandomData-5\": \"7\",\n" +
                "    \"detailFiledLength-5\": \"\",\n" +
                "    \"filedChainContent-5\": \"{\\\"chainCollPrefix-0\\\":\\\"4\\\",\\\"chainCollSuffix-0\\\":\\\"4\\\",\\\"chainPrefix-0\\\":\\\"4\\\",\\\"chainSuffix-0\\\":\\\"4\\\",\\\"chainFiledSeparator-0\\\":\\\"4\\\",\\\"chainRandomData-0\\\":\\\"1\\\",\\\"chainFiledLength-0\\\":\\\"\\\",\\\"chainRandomData-1\\\":\\\"1\\\",\\\"chainFiledLength-1\\\":\\\"\\\",\\\"chainRandomData-2\\\":\\\"4\\\",\\\"chainFiledLength-2\\\":\\\"\\\",\\\"chainRandomData-3\\\":\\\"3\\\",\\\"chainFiledLength-3\\\":\\\"\\\",\\\"chainAmountNumber-3\\\":\\\"1\\\"}\"\n" +
                "}";
        Map<String, String> map = JsonUtil.jsonToObject(json, Map.class);
        String chainJson = map.get("filedChainContent-5");
        Map chainMap = JsonUtil.jsonToObject(chainJson, Map.class);
        System.out.println(chainMap);

    }
}