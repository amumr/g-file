package com.example.gfile.util;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class ZipUtilTest {

    @Value("${root-directory}")
    private String rootDirectory;

    @Test
    void doZip() {
        File file = new File(rootDirectory);
        File[] files = file.listFiles();

        Stream<File> stream = Stream.of(files);
        List<String> fileList = stream.map(s -> s.getAbsolutePath()).collect(Collectors.toList());

        ZipUtil.doZip(fileList,rootDirectory+"123.zip");
    }

    @Test
    void unZip() {
    }
}